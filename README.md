TMS Docker Compose configuration
================================

This repository contains files that can be used to run an instance of <abbr title="Task Management System">TMS</abbr> with [Docker Compose](https://docs.docker.com/compose/).

- For production usage, see the [Production Environment Guide](production/README.md)
- For development, see the [Developer Environment Guide](development/README.md)